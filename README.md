# jira backup

Jira data is backed up by the ci server. The actual bash script is backup.sh

## configuration

The script requires a jira username and password with admin capabilities. Do not check these values into the repository.

USERNAME=
PASSWORD=

The script also uses the followign settings:

INSTANCE=wahoofitness.atlassian.net
LOCATION=

The backup zip file as of May, 2018 is approximately 1G in size.

